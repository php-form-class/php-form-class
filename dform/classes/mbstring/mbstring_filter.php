<?php
/* 
 * Web string validation and filter
 */

/* String validation interface */
interface imbstring_validate
{
    static function is_string($str);
    static function is_integer($str);
    static function is_double($str);
    static function is_md5($str);
    static function is_sha256($str);
    static function is_alnum($str);
    static function is_numeric($str);
    static function is_email($str);
    static function is_boolean($str);
    static function is_url($str);
    static function is_mobile($str);
    static function is_mphone($str);
    static function is_coordinate($str);
    static function is_size($str, $l = false, $h = false, $type = false, $size);
}

class mbstring_validate implements imbstring_validate
{
    static function is_size($str, $l = false, $h = false, $type = false, $size)
    {
        if (is_bool($str))
            return true;

        if ($type != 'string' && (is_int($str) || self::is_double($str)))
            $size = $str;

        return (($l && $size < $l) || ($h && $size > $h) ? false : true);
    }

    static function is_mobile($str)
    {
        return preg_match('/^(13[0-9]|15[0|1|3|6|7|8|9]|18[6|7|8|9])\d{8}$/', $str);
    }

    static function is_mphone($str)
    {
        $d = '';
        if (strpos($str, ";") !== false)
            $d = ";";

        if ($d) {
            foreach (explode($d, $str) as $_str) {
                if (!$_str)
                    continue;
                if (!self::_is_phone(trim($_str)))
                    return false;
            }
        } else
            return self::_is_phone($str);
        return true;
    }

    static function _is_phone($str)
    {
        return preg_match('/(^(0\d{2})-(\d{8})$)|(^(0\d{3})-(\d{7})$)|(^(0\d{2})-(\d{8})-(\d+)$)|(^(0\d{3})-(\d{7})-(\d+)$)/', $str);
    }

    static function is_coordinate($str)
    {
        return true;
    }

    static function is_string($str)
    {
        if ($str === '')
            return true;

        if (!is_string($str))
            return false;
        return true;
    }

    static function is_integer($str)
    {
        if (is_int($str) || (string) $str === (string) (int) $str)
            return true;
        return false;
    }

    static function is_double($str)
    {
        if (self::is_integer($str) || is_double($str))
            return true;
        return false;
    }

    static function is_numeric($str)
    {
        if (!is_numeric($str))
            return false;
        return true;
    }

    static function is_md5($str)
    {
        return preg_match('/^[a-z0-9]{32}$/i', $str) ? true : false;
    }

    static function is_alnum($str)
    {
        if (!preg_match('/^[a-z0-9]+$/i', $str))
            return false;
        return true;
    }

    static function is_email($str)
    {
        return preg_match('/^[A-Za-z0-9]+([._\-\+]*[A-Za-z0-9]+)*@([A-Za-z0-9]+[-A-Za-z0-9]*[A-Za-z0-9]*\.)+[A-Za-z0-9]+$/', $str) ? true : false;
    }

    static function is_sha256($str)
    {
        return preg_match('/^[a-z0-9]{64}$/i', $str) ? true : false;
    }

    static function is_url($str)
    {
        $regex = "((https?|ftp)\:\/\/)?"; // SCHEME
        $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
        $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
        $regex .= "(\:[0-9]{2,5})?"; // Port
        $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
        $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
        $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor
        return preg_match("/$regex/i", $str);
    }

    static function is_boolean($str)
    {
        return true;
    }
}

/* string filter */
interface imbstring_filter
{
    static function fmd5($str);
    static function fvar($str);
    static function fdouble($str);
    static function falnum($str);
    static function finteger($str);
    static function fescape($str, $quote_style = ENT_QUOTES, $encoding = 'UFT-8');
    static function funescape($str, $quote_style = ENT_QUOTES);
    static function fgpc($str);
    static function fboolean($str);
    static function furl($str);
    static function fhtmlname($str);
    static function fmphone($str);
    static function fcoordinate($str);
}

class mbstring_filter implements imbstring_filter
{
    static function fhtmlname($str)
    {
        return preg_replace('~[^a-z_0-9\-]*~i', '', $str);
    }

    static function furl($str)
    {
        return $str;
    }

    static function fmd5($str)
    {
        return preg_replace('~[^a-z0-9]*~i', '', $str);
    }

    static function fvar($str)
    {
        return preg_replace('~[^a-z_0-9]*~i', '', $str);
    }

    static function falnum($str)
    {
        return self::fmd5($str);
    }

    static function finteger($str)
    {
        return (int) $str;
    }

    /* Escape web string */
    static function fescape($str, $quote_style = ENT_QUOTES, $encoding = 'UTF-8')
    {
        return htmlspecialchars($str, $quote_style, $encoding);
    }

    /* Unescape */
    static function funescape($str, $quote_style = ENT_QUOTES)
    {
        return strtr($str, array_flip(get_html_translation_table(
            HTML_SPECIALCHARS, $quote_style)));
    }

    /* Double */
    static function fdouble($str)
    {
        return (double) $str;
    }

    /* Strip slashes */
    static function fgpc($str)
    {
        static $gpc = null;
        if (!isset($gpc))
            $gpc = @get_magic_quotes_gpc();
        return $gpc == 0 ? $str : stripslashes($str);
    }

    static function fmobile($str)
    {
        return substr(preg_replace('/[^0-9]*/i', '', $str), 0, 11);
    }

    static function fmphone($str)
    {
        return preg_replace('/ */', '', $str);
    }

    static function fboolean($str)
    {
        return (boolean) $str;
    }

    static function fcoordinate($str)
    {
        return preg_replace('/[^0-9\.,]*/', '', $str);
    }
}

?>
