<?php

if (!class_exists('mbstring', false))
    exit;

class mbstring_utf8 extends mbstring_base
{
    function __construct($init_str = '', $init_size = false, 
                         $method = '', $filter_name = null)
    {
        parent::__construct($init_str, $init_size, $method, $filter_name);
        $this->encoding = 'utf8';
    }

    protected function valid($str)
    {
        if ($str === '')
            return true;

        if (is_array($str))
            return true;

        if ($this->mb) {
            $ret =  mb_detect_encoding($str, 'UTF-8') ? 1 : 0;
            return $ret;
        } elseif ($this->iconv)
            return (iconv('UTF-8', 'UTF-8', $str) == $str) ? 1 : 0;

        /* http://www.w3.org/International/questions/qa-forms-utf-8.en.php */
        $rx = 
            '%^(?:'.
            '[\x09\x0A\x0D\x20-\x7E]|'.
            '[\xC2-\xDF][\x80-\xBF]|'.
            '\xE0[\xA0-\xBF][\x80-\xBF]|'.
            '[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}|'.
            '\xED[\x80-\x9F][\x80-\xBF]|'.
            '\xF0[\x90-\xBF][\x80-\xBF]{2}|'.
            '[\xF1-\xF3][\x80-\xBF]{3}|'.
            '\xF4[\x80-\x8F][\x80-\xBF]{2}'.
            ')*$%xs';
        return preg_match($rx, $str); 
    }

    protected function strlen($str)
    {
        /*
        if (is_int($str) || is_float($str))
            return $str;
         */

        if (is_array($str))
            return sizeof($str);

        if ($this->mb)
            return mb_strlen($str, 'UTF-8');
        if (function_exists('utf8_decode'))
            return strlen(utf8_decode($str));
        $pos = $real_len = 0;
        $len = strlen($text);
        $ret = '';

        /* max is 4 bytes */
        while ($pos < $len) {
            $ord = ord($text[$pos]) & 0xF0;

            # U-00000080  –  U-000007FF 
            if ($ord === 0xC0 || $ord === 0xD0)
                $pos += 2;
            # U-00000800 – U-0000FFFF
            else if ($ord === 0xE0)
                $pos += 3;
            # U-00010000 - U-7FFFFFFF
            # may greater than 4
            else if ($ord === 0xF0)
                $pos += 4;
            else
                ++$pos;

            ++$real_len;
        }

        return $real_len;
    }

    /* imbstring interface implements */
    function substr($start, $end = false)
    {
        if (!$this->string || ($start < 0 && $end < 0 && $end < $start))
            return '';

        if ($this->mb)
            return mb_substr($this->string, $start, $end, 'UTF-8');

        $match = array();
        # slow for large string ?
        preg_match_all("/./su", $this->string, $match);

        if (empty($match) || !isset($match))
            return '';

        $match1 = array_shift($match);
        $ostart = $start;
        if ($start < 0) {
            $match1 = array_reverse($match1);
            if ($end === false || $end == -1 || ($end > 0 && $end + $start >= 0)) {
                $end = abs($start);
                $start = 0;
            } else {
                $start = abs($end) - 1;
                $end = $end - $ostart + 1;
            }
        }

        if($end !== false)
            $match1 = array_slice($match1, $start, $end);
        else
            $match1 = array_slice($match1, $start);

        if ($ostart < 0)
            $match1 = array_reverse($match1);
        return implode('', $match1);
    }
}

