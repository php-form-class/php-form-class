<?php
/*
 * MBlite simple php framework.
 * Form helper class.
 * Autor: QiQi
 * Email: deblicn@gmail.com
 *  
 */

define('PACKAGE_FORM_DIR', dirname(__FILE__) . '/');

class form_validator
{
    public
        $form_name = '',
        $errors = array(),
        $validated = array(),
        $validate_type = array();

    protected 
        $ctrl = '',
        $model = '',
        $plugin_dir = '';

    function __construct()
    {
        $this->plugin_dir = PACKAGE_FORM_DIR . '/form/plugins/';
        $this->validate_type = @parse_ini_file(PACKAGE_FORM_DIR . 'form/reg.plugins', true);
        if (!$this->validate_type)
            throw new exception('No form validate plugin founded.');
    }

    function get_validate_type($type)
    {
        foreach ($this->validate_type as $valid_plugin => $valid_type) {
            if (strpos($valid_type, $type) !== false)
                return $valid_plugin;
        }
    }

    function load_plugin($type)
    {
        static $loaded_plugin = null;

        $plugin_name = $this->get_validate_type($type);
        if (!$plugin_name) {
            return;
        }

        if (isset($loaded_plugin[$plugin_name]))
            return $loaded_plugin[$plugin_name];

        $plugin_class= "form_validate_plugin_$plugin_name";
        if (class_exists($plugin_class, false)) {
            $loaded_plugin[$plugin_name] = new $plugin_class;
            return $loaded_plugin[$plugin_name];
        }

        $plugin_file = $this->plugin_dir . 'validate.' . $plugin_name . '.php';
        if (!file_exists($plugin_file))
            return;
        require $plugin_file;

        $loaded_plugin[$plugin_name] = new $plugin_class;
        return $loaded_plugin[$plugin_name];
    }

    function validate($form_infos ,$submit_method = 'post', $ctrl = '', $method = '')
    {
        if (!isset($form_infos))
            return true;

        $error = false;
        $this->errors = array();

        $extra_validation = array();

        foreach ($form_infos as $var_name => $form_info) {
            if ($var_name[0] == 'Z')
                continue;

            if ($var_name == '#extra') {
                $extra_validation = $form_info;
                continue;
            }

            if (isset($form_info['valid']) && !$form_info['valid']) {
                $this->validated[$var_name]['val'] = isset($form_info['default_value']) ? $form_info['default_value'] : '';
                $this->validated[$var_name]['type'] = $form_info['type'];
                continue;
            }

            if (!isset($form_info['type'])) {
                $error = true;
                continue;
            }
            $validate_plugin = $this->load_plugin($form_info['type']);
            if (!$validate_plugin)
                $valid = null;
            else
                $valid = $validate_plugin->valid($var_name, $form_info);

            $this->validated[$var_name]['type'] = $form_info['type'];
            if ($valid === null || $valid < 0) {
            //    $this->errors[$var_name] = $form_info;
                $this->errors[$var_name] = $valid ? $valid : true;
                $this->validated[$var_name]['val'] = '';
                $error = true;
                continue;
            }

            $this->validated[$var_name]['val'] = $valid;
        }

        if ($extra_validation) {
            if (!$this->extra_validation($extra_validation))
                $error = true;
        }

        return $error;
    }

    protected function extra_validation($extra_validation)
    {
        if (!$extra_validation)
            return true;
        foreach ($extra_validation as $type => $info) {
            switch ($type) {
            case 'compare':
                @list($arg1, $arg2) = $info['fields'];

                if (!$arg1 || !$arg2)
                    continue;
                if (!isset($this->validated[$arg1]) || !isset($this->validated[$arg2]))
                    continue;
                $valid = false;
                switch ($info['type']) {
                case '=':
                    $valid = $this->validated[$arg1]['val'] == $this->validated[$arg2]['val'];
                    break;

                case '>':
                    $valid = $this->validated[$arg1]['val'] > $this->validated[$arg2]['val'];
                    break;

                case '<':
                    $valid = $this->validated[$arg1]['val'] < $this->validated[$arg2]['val'];
                    break;
                }

                if (!$valid) {
                    $this->errors[$arg2] = true;
                }
                break;

            case 'redup_check':
                $ctrl = $this->ctrl;
                $model = $this->model;
                if (!$ctrl || !$model || !isset($ctrl->$model))
                    continue;

                $field = @array_shift($info);
                if (!$ctrl->$model->redup_check($field))
                    $this->errors[$field] = true;

                break;
            }
        }
    }

}

abstract class form_validate_plugin
{
    function __construct()
    {
    }

    abstract function valid();
}

class DForm
{
    protected
        $autohide_extra = false,
        $validated = array(),
        $validator = null,
        $errors    = array();

    public
        $debug = false,
        $form_info = array();

    function __construct()
    {
        $this->validator = new form_validator;
    }

    function set($key, $val)
    {
        if (!isset($this->$key))
            return;

        $this->$key = $val;
    }

    function render($form_name = '')
    {
        if (!isset($this->form_info[$form_name]))
            return '';

        $form_info = $this->form_info[$form_name];

        require_once PACKAGE_FORM_DIR . 'form/widgets.php';

        $method = isset($form_info['base_info']['method']) ? 
            $form_info['base_info']['method'] : 'post';
        $upload = isset($form_info['base_info']['upload']) ? 
            (bool) $form_info['base_info']['upload'] : false;
        $upload = $upload ? ' enctype="multipart/form-data"' : '';
        $onsubmit = isset($form_info['base_info']['onsubmit']) ? 
            $form_info['base_info']['onsubmit'] : '';
        $class  = isset($form_info['base_info']['class']) ? 
            $form_info['base_info']['class'] : '';
        $layout = isset($form_info['base_info']['layout']) ?
            $form_info['base_info']['layout'] : 'p';

        /*
        $form_string = '<form name="'. $form_name. '" method="' . $method . '"' 
            .$upload . ' ' . ($onsubmit ? ' onsubmit="' . $onsubmit . '"' : '').
            ($class ? ' class="'. $class. '"' : ''). '>';
         */
        $layout_method = "as_$layout";
        if (!method_exists($this, $layout_method))
            throw new exception('Form layout not supported!');

        $form_string = '';

        $extra_form_string = '';
        foreach ($form_info as $name => $field) {
            if (!isset($field['widget_info']))
                continue;

            $field['name'] = $name;
            $widget = form_widget::_($field['widget_info']['type'], $field);
            if (!$widget)
                continue;

            $_t = $widget->render();
            $_t = $this->$layout_method($_t);

            if (!isset($field['widget_info']['required']) || !$field['widget_info']['required'])
                $extra_form_string .= $_t;
            else
                $form_string .= $_t . "\n";
        }

        if ($this->autohide_extra && $extra_form_string) {
            $div_widget = new widget_hidden_roll_div;
            $extra_form_string = $div_widget->render($extra_form_string);
        }

        if ($this->debug)
            echo $form_string. $extra_form_string;

        return $form_string. $extra_form_string;
    }

    function as_p($string)
    {
        return "<p>\n$string\n</p>";
    }

    function set_info($info = array())
    {
        $this->form_info = $info;
    }

    function append_form_info($key, $val = '', $sort = true)
    {
        if (is_array($key)) {
            $this->form_info = array_merge_recursive($this->form_info, $key);
            return;
        }

        if (isset($this->form_info[$key]))
            $this->form_info[$key] += $val;
    }

    protected function get_error_type($type)
    {
        switch ($type) {
        case 'string':
            return '字符';
        case 'integer':
            return '数字';
        case 'double':
        case 'float':
            return '浮点数';
        case 'url':
            return '标准url地址';
        case 'image':
            return '图片';
        case 'mphone':
            return '电话号码';
        case 'mobile':
            return '手机号码';
        }

        return '任意';
    }

    function get_errors_string($options = array())
    {
        if (!$this->validator->errors)
            return '';

        $string = "<div class=\"error-string\"><ul>\n";

        $_t_form = isset($this->form_info[$this->form_name]) ? $this->form_info[$this->form_name] : false;
        foreach ($this->validator->errors as $error => $v) {
            $prefix = isset($options['prefix']) ? $options['prefix'] : '请填写';
            $suffix = isset($options['suffix']) ? $options['suffix'] : '，正确的格式是 : ';
            $_t = '';

            if ($_t_form) {
                $_t = (isset($_t_form[$error]['min_size']) ? 
                    $_t_form[$error]['min_size'] . '-'. 
                    $_t_form[$error]['max_size']. ' 位的' : '') . 
                    $this->get_error_type($_t_form[$error]['type']);

                if ($_t_form[$error]['type'] == 'image') {
                    $prefix = '';
                    $suffix = '上传失败';
                    $_t = '';
                }
            }
            $string .= "<li>$prefix{$_t_form[$error]['label']}$suffix$_t</li>\n";
        }

        $string .= "</ul></div>\n";
        return $string;
    }

    function set_validated($data)
    {
        if (!$data)
            return;

        foreach ($data as $_key => $_data) {
            $this->validated[$_key]['val'] = $_data;
        }
    }

    function get_validated()
    {
        return $this->validator->validated;
    }

    function get_errors()
    {
        return $this->validator->errors;
    }

    function validate($name, $method = 'post', $ctrl = '', $model = '')
    {
        $this->form_name = $name;
        if (!isset($this->form_info[$name]))
            return true;

        $ret = $this->validator->validate($this->form_info[$name], $method, $ctrl, $model);
        //unset($this->form_info[$name]);
        return $ret;
    }

    function do_form($options)
    {
        $valid_name = isset($options['valid_name']) ? $options['valid_name'] : '';
        if (!$valid_name)
            return -1;

        $method         = isset($options['method']) ? $options['method'] : 'post';
        $set_error      = isset($options['set_error']) ? (boolean) $options['set_error'] : true;
        $onerror        = isset($options['onerror']) ? $options['onerror'] : '';
        $onerror_args   = isset($options['onerror_args']) ? $options['onerror_args'] : null;
        $onsuccess      = isset($options['onsuccess']) ? $options['onsuccess'] : '';
        $onsuccess_args = isset($options['onsuccess_args']) ? $options['onsuccess_args'] : '';
        $msg            = isset($options['msg']) ? $options['msg'] : '';
        $redirect       = isset($options['redirect']) ? $options['redirect'] : '';
        $model          = isset($options['model']) ? $options['model'] : '';
        $model          = $model ? $model : 'model';
        $ctrl           = isset($options['ctrl']) ? $options['ctrl'] : '';

        $r_method       = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';

        if ($model != 'model') {
            $omodel = $model;
            $model = 'model_' . $model;
            if (!isset($ctrl->$model))
                $ctrl->$model = loader::load_object($omodel, 'model');
            if (!$ctrl->$model)
                return -10;
        }

        if (!$r_method || strtolower($r_method) != strtolower($method) ||
            $this->validate($valid_name, $method, $ctrl, $model))
        {
            if (!$ctrl)
                return -8;

            if ($set_error)
                $ctrl->set_error_view();

            if (!$onerror || !method_exists($ctrl, $onerror))
                return -2;

            if ($onerror_args) {
                call_user_func_array(array($ctrl, $onerror), $onerror_args);
                return -3;
            }
            call_user_func(array($ctrl, $onerror));
            return -5;
        }

        if (!$onsuccess || !method_exists($ctrl->$model, $onsuccess))
            return -4;

        $args = array();
        if ($onsuccess_args) {
            foreach ($onsuccess_args as $v) {
                $args[] = $v;
            }
        }

        $fields = $data = array();
        $i = 0;
        $sub_modules = !empty($ctrl->model->sub_modules);
        if ($sub_modules) {
            $cur_sub_name = key($ctrl->model->sub_modules);
            $cur_sub_sep = current($ctrl->model->sub_modules);
            $fields['main'] = array();
            $data['main'] = array();
            $fields[$cur_sub_name] = array();
            $data[$cur_sub_name] = array();
        }

        $new = false;
        foreach ($this->validator->validated as $field => $v) {
            if ($sub_modules && $field == $cur_sub_sep)
                $new = true;

            if ($sub_modules && !$new) {
                $fields['main'][$field] = $v['type'];
                $data['main'][$field] = $v['val'];
                continue;
            }

            if ($new) {
                $fields[$cur_sub_name][$field] = $v['type'];
                $data[$cur_sub_name][$field] = $v['val'];
                continue;
            }
            $fields[$field] = $v['type'];
            $data[$field] = $v['val'];
        }
        $args[] = $data;
        $args[] = $fields;

        $ret = call_user_func_array(
            array($ctrl->$model, $onsuccess),
            $args);

        if (!$redirect)
            return $ret;

        if ($msg) {
            if ($ret && isset($redirect[0]) && $redirect[0]) {
                $ctrl->message($msg[0], $redirect[0]);
				return;
			}

            if (!$ret && $redirect && isset($redirect[1]) && $redirect[1]) {
                $ctrl->message($msg[1], $redirect[1]);
				return;
			}
        }

		return $ret;

    }
}

class form_lang
{
    public $lang;
    public $strings;

    public function __construct($lang = 'zh-cn')
    {
        $this->lang = $lang;
        $this->strings['zh-cn'] = array('char' => '字符',
                                        'numberic' => '数字',
                                       );
    }

    public function _($str = '')
    {
        if (!$str)
            return '';

        return (isset($this->strings[$this->lang][$str]) ?
                $this->strings[$this->lang][$str] : $str);
    }
}

?>
