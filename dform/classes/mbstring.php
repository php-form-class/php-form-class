<?php
/*
 * MBlite simple php framework.
 * MBlite mbstring
 * Autor: QiQi
 * Email: deblicn@gmail.com
 *  
 */

define('MBSTRING_ROOT', dirname(__FILE__). '/mbstring/');

class mbstring
{
    static function m($names, &$invalids, $method = 'post')
    {
        if (!is_array($names))
            return;

        $invalids = $valids = array();
        foreach ($names as $key => $data) {
            $pds = array();
            if (!preg_match('~([a-z]*):([0-9]*)\-([0-9]*):*([a-z0-9]*)~i',
                $data, $pds) || !isset($pds[1]))
                continue;
            $t = $pds[1];
            $l = isset($pds[2]) ? (int) $pds[2] : false;
            $u = isset($pds[3]) ? (int) $pds[3] : false;
            $d = isset($pds[4]) ? $pds[4] : null;

            $v = self::_($key, array('method' => $method));
            if (!$v->validation($t) || ($l && !$v->validation('size', $l, $u, $t))) 
            {
                $invalids[] = $key;
                if (!is_null($d))
                    $valids[$key] = $d;

                continue;
            }

            $valids[$key] = $v->filter($t);
        }
        return $valids;
    }

    static function &_($init, $opt = null)
    {
        static $included = null;
        static $obj = null;

        $init       = !is_null($init) ? $init : '';
        $init_size  = isset($opt['init_size']) ? (bool)$opt['init_size'] : true;
        $encoding   = isset($opt['encoding']) ? $opt['encoding'] : 'utf8';
        $method     = isset($opt['method']) ? $opt['method'] : '';
        $cls        = 'mbstring_'. $encoding;

        $null       = null;

        if (!isset($included)) {
            $file = dirname(__FILE__). '/'. 
                substr(basename(__FILE__), 0, strpos(basename(__FILE__), '.')). 
                '/'. $encoding. '.php';
            if (!file_exists($file))
                return $null;
            require $file;
            $included = true;
        }

        if (!class_exists($cls, false))
            return $null;

        if (!isset($obj))
            $obj = new $cls($init, $init_size, $method);
        else
            $obj->init_str($init, $init_size, $method);

        return $obj;
    }
}

/* string interface */
interface imbstring
{
    public function substr($start, $num = false);
    public function set_filter($filter_obj);
    public function filter();
    public function validation();
}

/* MungBean string base object */
abstract class mbstring_base implements imbstring
{
    public $string = '', $encoding = '';
    public $size, $iconv = false, $mb = false;
    public $filter, $validate;
    protected static
        $place = array('get', 'post', 'request', 'server',
                        'session', 'cookie', 'files');

    function __construct($init_str = '', $init_size = true, 
                         $method = '', $filter_name = null)
    {
        $this->iconv    = function_exists('iconv') ? true : false;
        $this->mb       = function_exists('mb_detect_encoding') ? true : false;
        $this->init_str($init_str, $init_size, $method);
        //    if ($this->string !== '')
        $this->set_filter($filter_name);
    }

    public function init_str($init_str = '', $init_size = true, $method = '')
    {
        $this->string   = $method ? $this->get_web_var($method, $init_str) : 
            (($init_str !== '' && $this->valid($init_str)) ? $init_str : '');

        if ($init_size) {
            if ($this->string === '')
                $this->size = 0;
            else
                $this->size = $this->strlen($this->string);
        }
    }

    protected function get_web_var($method, $key = '')
    {
        if (!in_array($method, self::$place) || !$key)
            return '';

        $var = '';

        switch ($method) {
        case 'get':
            $var = isset($_GET[$key]) ? $_GET[$key] : '';
            break;
        case 'post':
            $var = isset($_POST[$key]) ? $_POST[$key] : '';
            break;
        case 'server':
            $var = isset($_SERVER[$key]) ? $_SERVER[$key] : '';
            break;
        case 'request':
            $var = isset($_REQUEST[$key]) ? $_REQUEST[$key] : '';
            break;
        default:
            $var = '';
            break;
        }

        return $var;
    }

    function set_filter($filter_name)
    {
        if (!$filter_name)
            $filter_name = 'mbstring';

        $validate_name  = $filter_name . '_validate';
        $filter_name    .= '_filter';
        if (!class_exists($filter_name))
            require MBSTRING_ROOT . $filter_name . '.php';

        if (!class_exists($filter_name))
            return;
        $this->filter = $filter_name;
        $this->validate  = $validate_name;
    }

    function validation()
    {
        $oargs = func_get_args();
        if (sizeof ($oargs) == 0)
            return;

        if (strpos($oargs[0], 'array_') !== false)
            return true;

        $method = 'is_'. $oargs[0];
        $oargs[0] = $this->string;
        if (strpos($method, 'size') !== false) {
            $oargs[] = $this->size;
        }


        if (method_exists($this->validate, $method)) {
            if (is_array($oargs[0]))
                return true;
            return call_user_func_array(array($this->validate, $method), $oargs);
        }
    }

    function filter()
    {
        $args = func_get_args();
        if (sizeof ($args) == 0)
            return;

        $methods = explode(',', $args[0]);

        if (!$methods)
            return;

        $args[0] = $this->string;
        $ret = $args[0];
        foreach ($methods as $method) {
            if (strpos($method, 'array_') !== false)
                $method = str_replace('array_', '', $method);
            $method = 'f'. trim($method);
            if (method_exists($this->filter, $method)) {
                if (is_array($args[0])) {
                    $ret = array_map(array($this->filter, $method), $args[0]);
                } else {
                    $ret = call_user_func_array(array($this->filter, $method), $args);
                }
                $args[0] = $ret;
            } 
        }
        return $ret;
    }

    function __tostring()
    {
        return (string) $this->string;
    }

    abstract protected function valid($str);
    abstract protected function strlen($str);
}


/* testing
    
echo '<pre>';
$a = pmbstring::_('asdfasdf');
echo ($b = $a->substr(0, 2)) . "\n";
echo memory_get_usage(). "\n";
for ($i = 0; $i < 40; $i++) {
    pmbstring::_('asdfasdf'. $i);
}
echo $a->filter('integer'). '<br />';
echo $a->filter('double'). '<br />';
var_dump($a->validation('md5'));
echo memory_get_usage(). "\n";
echo '</pre>';


/* Its done!
 */
