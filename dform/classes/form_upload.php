<?php

define('DEF_ALLOW_EXTENSION', 'jpg:jpeg:gif:png');

class form_upload
{
    static $self;

    public $file = '';

    public $tmp_file = '';

    public $file_extension = '';
    public $file_mimetype = '';
    public $file_path = '';

    public $local;

    public $file_size = 0;
    public $file_width = 0;
    public $file_height = 0;

    public $max_filesize = 0;
    public $max_width = 0;
    public $max_height = 0;

    public $upload_dir = '';

    public $target_file = '';

    public $filename_format = 'unique';
    public $define_name = '';
    
    public $allowed_extension = '';

    public $file_moved = false;
    public $error = array();

    protected
        $clean_mode = '';

    static function _($upload_dir = '', $allowed_extension = '',
                      $max_width = 4096, $max_height = 4096, $file_max = 8192000)
    {
        if (!isset(self::$self)) {
            $c = __CLASS__;
            self::$self = new $c($upload_dir, $allowed_extension, $max_width, $max_height, $file_max);
        }

        self::$self->error = array();
        return self::$self;
    }

    function __construct($upload_dir = '', $allowed_extension = '',
                         $max_width = 4096, $max_height = 4096, $file_max = 8192000)
    {
        $destination = trim($upload_dir);
        if (($dl = strlen($destination)) > 0 &&
            ($destination[$dl - 1] == '/' ||
             $destination[$dl - 1] == '\\'))
            $destination = substr($destination, 0, $dl - 1);

        $destination = str_replace(array('../', '..\\', './', '.\\'), '', $destination);

        if ($destination && ($destination[0] == '/' || $destination[0] == "\\"))
            $destination = '';
        
        $_config_dir = '';
        if (stripos(PHP_OS, 'win') !== false && isset($_SERVER['DOCUMENT_ROOT']))
            $_config_dir = $_SERVER['DOCUMENT_ROOT'];
        else
            $_config_dir = MB_DR != '/' ?
                str_replace(MB_DR, '',
                            str_replace('\\', '/', MB_WORK_ROOT)):
                MB_WORK_ROOT;
        $config_dir = $_config_dir . '/'. $destination;

        if (!$config_dir)
            $config_dir = './';

        $this->upload_dir =  $config_dir . '/';

        // Set allowed extensions
        $this->allowed_extension = (empty($allowed_extension)) ?
            DEF_ALLOW_EXTENSION : $allowed_extension;

        // Set allowed dimensions.
        if (is_int($max_width) && $max_width > 0)
            $this->max_width = $max_width;
        if (is_int($max_height) && $max_height > 0)
            $this->max_height = $max_height;

        $ini_max = @ini_get('upload_max_filesize');
        if (!empty($ini_max)) {
            $last = strtolower($ini_max[strlen($ini_max)-1]);
            $ini_max = substr($ini_max, 0, -1);
            switch($last) {
            case 'g':
                $ini_max *= 1024;
            case 'm':
                $ini_max *= 1024;
            case 'k':
                $ini_max *= 1024;
            }
        } else
            $ini_max = 1024;

        if (is_int($file_max) && $file_max > 0 && $file_max <= $ini_max)
            $this->max_filesize = $file_max;
        else
            $this->max_filesize = $ini_max;

        return true;
    }

    /**
     * Set allowed dimension.
     */
    function set_allowed_dimension($max_width, $max_height, $max_filesize)
    {
        $this->max_width = (int) $max_width;
        $this->max_height = (int) $max_height;
        $this->max_filesize = (int) $max_filesize;

        return true;
    }

    /**
     * Get file info.
     */
    function filespec($upload_ary) 
    {
        $this->file = trim(htmlspecialchars(basename($upload_ary['name'])));
        $this->tmp_file = $upload_ary['tmp_name'];
        $this->file_size = $upload_ary['size'];
        $this->file_mimetype = $upload_ary['type'];

        // Opera adds the name to the mime type
        $this->file_mimetype = (strpos($this->file_mimetype, '; name') !== false) ? str_replace(strstr($this->file_mimetype, '; name'), '', $this->file_mimetype) : $this->file_mimetype;
        if (!$this->file_mimetype) {
            $this->file_mimetype = 'application/octetstream';
        }
        $this->file_extension = $this->get_extension($this->file);

        // Try get real file size
        $this->file_size = ($ret = @filesize($this->tmp_file)) ? $ret : $this->file_size;

        $this->file_width = $this->file_height = 0;
        $this->file_moved = false;
        $this->local = (isset($upload_ary['local_mode'])) ? true : false;

        return true;
    }

    /**
     * Set filename format
     */
    function set_filename_format($mode = '', $define_name = '')
    {
        $this->filename_format = (empty($mode)) ? 'unique' : $mode;
        if ($define_name)
            $this->define_name = $define_name;
    }

    /** 
     * Clean the file name.
     */
    function clean_filename($mode = '', $prefix = '')
    {
        if (!empty($this->error)) {
            return;
        }
        $mode = (!empty($mode)) ? $mode : (empty($this->filename_format)) ? 'unique' : $this->filename_format;

        $this->clean_mode = $mode;

        switch ($mode) {
        case 'real':
            // Remove every extension from filename (to not let the mime bug being exposed)
            if (strpos($this->file, '.') !== false)
                $this->file = substr($this->file, 0, strpos($this->file, '.'));

            // Replace any chars which may cause us problems with _
            $bad_chars = array("'", "\\", ' ', '/', ':', '*', '?', '"', '<', '>', '|');
            $this->file = rawurlencode(str_replace($bad_chars, '_', strtolower($this->file)));
            $this->file = preg_replace("/%(\w{2})/", '_', $this->file);
            $this->file = $prefix . $this->file. '.'. $this->file_extension;

            break;

        case 'define':
            $this->file = $this->define_name . '.' . $this->file_extension;
            break;
        case 'unique':
        default:
            $this->file = $prefix . md5(uniqid(rand(), true)). '.'. $this->file_extension;
        }

        return true;
    }


    function get_extension($filename)
    {
        if (strpos($filename, '.') === FALSE) {
            return '';
        }
        $filename = explode('.', $filename);

        return strtolower(array_pop($filename));
    }

    function get($property)
    {
        if (!isset($this->$property)) {
            return false;
        }

        return $this->$property;
    }

    /**
     * From form upload process, from p
     */
    function do_upload($form_name)
    {
        unset($_FILES[$form_name]['local_mode']);
        if (!$this->is_valid($form_name)) {
            $this->error[] = "Upload form not set.";
            return $this->error;
        }

        // Get upload file information.        
        $this->filespec($_FILES[$form_name]);

        // Error array seted ?

        if (isset($_FILES[$form_name]['error'])) {
            $error = $_FILES[$form_name]['error'];
            if ($error !== UPLOAD_ERR_OK) {
                $this->error[] = $error;
                return $this->error;
            }
        }

        // Upload file empty ?
        if (isset($_FILES[$form_name]['size']) && $_FILES[$form_name]['size'] == 0) {
            $this->error[] = "Upload file empty.";
            return $this->error;
        }

        // PHP Upload filesize exceeded
        if ($this->get('filename') == 'none') {
            $this->error[] = "Size Overrun :" . @ini_get('upload_max_filesize');
            return $this->error;
        }

        
        // Not correctly uploaded
        if (!$this->is_uploaded()) {
            $this->error[] = "Not uploaded.";
            return $this->error;
        }

        $this->common_checks();

        if (!empty($this->error)) {
            return $this->error;
        }

        // Move file .
        $this->move_file();
        if (!empty($this->error))
            return $this->error;

        return true;
    }

    function common_checks()
    {
        // Filesize is too big or it's 0 if it was larger than the maxsize 
        // in the upload form
        if ($this->max_filesize && ($this->get('file_size') > $this->max_filesize || $this->get('file_size') == 0)) {
            $size_lang = ($this->max_filesize >= 1048576) ? 'M' : (($this->max_filesize >= 1024) ? 'KB' : 'B' );
            $max_filesize = ($this->max_filesize >= 1048576) ? round($this->max_filesize / 1048576 * 100) / 100 : (($this->max_filesize >= 1024) ? round($this->max_filesize / 1024 * 100) / 100 : $this->max_filesize);

            $this->error[] = "File too big, Max size is : $max_filesize $size_lang";
        }

        // Check file name
        if (preg_match("#[\\/*?\"<>|]#i", $this->get('file')))
            $this->error[] = "Invalid file name: ".$this->get('file');

        // Invalid Extension
        if (!$this->valid_extension())
            $this->error[] = "Invalide file extension: ".$this->get('file_extension');

        return true;
    }

    function is_uploaded()
    {
        /*
         if (!$this->local && !is_uploaded_file($this->tmp_file)) {
         return false;
         }

         if ($this->local && !file_exists($this->tmp_file)) {
         return false;
         }

        */

        return (is_uploaded_file($this->tmp_file));
    }

    function valid_extension()
    {
        return (strpos($this->allowed_extension, $this->get('file_extension')) !== false) ?
            true : false;
    }

    function valid_dimension()
    {
        if (!$this->max_filesize && !$this->max_width && !$this->max_height) {
            return true;
        }

        if ($this->get('file_size') > $this->max_filesize ||
            $this->get('file_width') > $this->max_width ||
            $this->get('file_height') > $this->max_height) 
            {
                $this->error[] = "Invalid dimension";
                return false;
            }

        return true;
    }

    function is_valid($form_name)
    {
        return (isset($_FILES[$form_name]) && $_FILES[$form_name]['name'] != 'none') ? true : false;
    }

    function move_file($chmod = 0666)
    {
        if (sizeof($this->error))
            return false;

        // setting target file.
        if (!$this->set_target_file()) {
            $this->error[] = 'Could not create hash directory.';
            return false;
        }

        if (empty($this->target_file)) {
            $this->error[] = "Cannt set target file.";
            return false;
        }
        // Move file.
        if (!@move_uploaded_file($this->tmp_file, $this->target_file)) {
            if (!@copy($this->tmp_file, $this->target_file)) {
                $this->error[] = "Cannot cp or mv target_file: ".$this->target_file;
                return false;
            } else
                @unlink($this->tmp_file);
        }

        // chmod uploadded file
        @chmod($this->target_file, $chmod);

        // Try to get real filesize from target file
        $this->filesize = (@filesize($this->target_file)) ? 
            @filesize($this->target_file) : $this->file_size;

        if ($this->is_image())
            list($this->file_width, $this->file_height) = @getimagesize($this->target_file);

        $this->file_moved = true;
        $this->additional_checks();

        return true;
    }

    /**
     *  Additional checks
     */
    function additional_checks()
    {
        if (!$this->file_moved)
            return false;

        // Filesize is too big or it's 0
        if ($this->max_filesize && ($this->get('filesize') > $this->max_filesize || $this->filesize == 0))
            {
                $size_lang = ($this->max_filesize >= 1048576) ? 'MB' : (($this->max_filesize >= 1024) ? 'KB' : 'BYTES' );
                $max_filesize = ($this->max_filesize >= 1048576) ? round($this->max_filesize / 1048576 * 100) / 100 : (($this->max_filesize >= 1024) ? round($thisd->max_filesize / 1024 * 100) / 100 : $this->max_filesize);
                $this->error[] = "Upload file size : ".$this->filesize." too big, max size is : ".$max_filesize. " $size_lang.";
                return false;
            }

        if (!$this->valid_dimension())
            $this->error[] = "Wrong size: ".$this->max_width . '- '. $this->max_height;
    }

    function set_target_file($mode = '')
    {
        $this->clean_filename($mode);
        $dirs = $this->get_file_hash_dir(basename($this->file));
        if (!($hashed_dir = $this->mk_hash_dir($dirs)))
            return false;

        $this->file_path = $dirs[0] . '/' . $dirs[1] . '/';
        $this->target_file = $hashed_dir . basename($this->file);
        return true;
    }

    function get_file_hash_dir($file_name)
    {
        $hash = $file_name;
        if ($this->clean_mode != 'unique')
            $hash = md5($file_name);

        $dir1 = substr($hash, 0, 2);
        $dir2 = substr($hash, 2, 2);
        return array($dir1, $dir2);
    }

    function mk_hash_dir($dirs)
    {
        if (!$dirs)
            return;

        $_dir = $this->upload_dir;
        foreach ($dirs as $dir) {
            $_dir .= $dir . '/';
            if (file_exists($_dir) && is_dir($_dir))
                continue;
            if (!@mkdir($_dir))
                return;
        }
        return $_dir;
    }
    
    function is_image()
    {
        return (strpos($this->file_mimetype, 'image/') !== false) ? 
            true : false;
    }

    function remove()
    {
        if ($this->file_moved)
            @unlink($this->target_file);
        if (file_exists($this->tmp_file) && !$this->file_moved)
            @unlink($this->tmp_file);
    }

    function is_error()
    {
        return (empty($this->error)) ? false : true;
    }

}

?>
