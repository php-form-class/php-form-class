<?php

define('MB_DR', dirname(__FILE__) .'/../../');

class form_widget
{
    static function _($widget, $attrib)
    {
        $widget_class = 'form_widget_input_' . $widget;
        if (!class_exists($widget_class, false))
            return;

        return new $widget_class($attrib);
    }
}

abstract html_widget
{
    protected $html_escape = 'htmlspecailchars';
    protected $html_escape_opt = defined('HTML5_QUOTES') ? HTML5_QUOTES : ENT_QUOTES;
    protected $html_escape_charset = 'UTF-8';
    protected $if_isset_ary = '<?php if (isset($%s["%s"])) { %s; } else { %s;}';
    protected $echo_isset_ary = "<?php if (isset($%s[\"%s\"])) { echo $this->html_escape(%s, $this->html_escape_opt, $this->html_escape_charset); }";
    protected $if_isset_start = "<?php if (isset($%s)) { ?>";
    protected $if_stop = "<?php } ?>";

    abstract public function render();
}

class widget_hidden_roll_div extends html_widget
{
    function render($string = '', $label = '编辑附加信息')
    {
        return '<h3 class="title" onclick="Effect.toggle(\'extra_form_info\', \'blind\', { duration: 0.5 });">'.
           "\n". $label. '</h3> <div id="extra_form_info" style="display:none;">'. "\n".  $string. "\n</div>";
    }
}


abstract class form_widget_base extends html_widget
{
    protected $attrib = array();

    abstract function _render();
    abstract function _after();

    function __construct($attrib)
    {
        $this->set_attrib($attrib);
    }

    protected function set_attrib($name, $val = null)
    {
        if (is_array($name))
            $this->attrib  = $name;
        else
            $this->attrib[$name] = $val;
    }

    protected function get_default_id()
    {
        return isset($this->attrib['widget_info']['id']) ? $this->attrib['widget_info']['id'] : $this->attrib['name'];
    }

    protected function gen_inline_js($js_code)
    {
        return sprintf('<script type="text/javascript">'."\n".'//<![CDATA['."\n"."%s\n//]]>\n</script>", $js_code);
    }

    protected function gen_src_js($js_src)
    {
        return sprintf('<script type="text/javascript" src="%s"></script>', $js_src);
    }

    protected function gen_label()
    {
        return sprintf('<label for="%s">%s %s</label>', $this->get_default_id(), (isset($this->attrib['label']) ? $this->attrib['label'] : $this->get_default_id()), $this->gen_required());
    }

    protected function gen_attrib($name, $attrib_name = '')
    {
        $attrib_name = $attrib_name ? $attrib_name : $name;
        if ($name == 'id') {
            $this->attrib['widget_info']['id'] = $this->get_default_id();
        }
        if ($name == 'name')
            $rname  =  isset($this->attrib[$name]) ? $this->attrib[$name] : '';
        else
            $rname  =  isset($this->attrib['widget_info'][$name]) ? $this->attrib['widget_info'][$name] : '';
        $rattrib = $rname ? ' ' . $attrib_name.'="'.$rname.'"' : '';
        if ($name == 'class') {
            $rattrib = sprintf($this->php_isset_ary, 'invalid', $this->attrib['name'], ' class="form-error" ', $ratrrib);//{if $#invalid.'.$this->attrib['name'].'#$} class="form-error"{else} '.$rattrib.'{/if}';
        } 
        return $rattrib;
    }

    protected function gen_value($unescape = false)
    {
        $value = isset($this->attrib['widget_info']['value']) ? $this->attrib['widget_info']['value'] : '';
        /*
        $tpl_value = '{$valid.' 
            .$this->attrib['name'] . '.val}'.$value;
        */
        $tpl_value = sprintf($this->echo_isset_ary, 'valid', $this->attrib['name'],
                             'valid['. $this->attrib['name']. '][val]');
        return $tpl_value;
    }

    protected function gen_required()
    {
        if (!isset($this->attrib['widget_info']['required']) || !$this->attrib['widget_info']['required'])
            return '';
        return '<strong> *</strong>';
    }
}

class form_widget_input extends form_widget_base
{
    function render()
    {
        return $this->_render() . $this->_after();
    }

    function _render()
    {
        return '';
    }

    function _after()
    {
        return  $this->gen_tip() . "\n". $this->gen_form_error();
    }
    
    protected function gen_tip()
    {
        return (isset($this->attrib['widget_info']['tip']) ? 
            '<span class="form-tip">'.
            mbstring::_($this->attrib['widget_info']['tip'])->filter('escape').
            '</span>' . "\n" :
            '');
    }

    protected function gen_form_error($as_block = 'div', $class = '')
    {
        if (!isset($this->attrib['name']))
            return '';

        $class = $class ? $class : 'notice';
        $err_string = $this->gen_error_string();
        return (sprintf($this->if_isset_start, $this->attrib['name']). "<$as_block class=\"$class\">$err_string</$as_block>\n". $this->if_stop;
    }
    
    protected function get_error_type($type)
    {
        switch ($type) {
        case 'string':
            return '字符';
        case 'integer':
            return '数字';
        case 'double':
        case 'float':
            return '浮点数';
        case 'url':
            return '标准url地址';
        case 'image':
            return '图片';
        case 'mphone':
            return '电话号码';
        case 'mobile':
            return '手机号码';

        }

        return '任意';
    }

    protected function gen_error_string()
    {
        if (isset($this->attrib['error_string']))
            return $this->attirb['error_string'];

        if ($this->attrib['type'] == 'image')
            return $this->get_error_type($this->attrib['type']) . '上传失败';

        /*
        if (!isset($this->attrib['min_size']))
            return '';
         */

        $prefix = isset($this->attrib['prefix']) ? $this->attrib['prefix'] : '请填写';
        $suffix = isset($this->attrib['suffix']) ? $this->attrib['suffix'] : '，正确的格式是 : ';
        $_t = (!isset($this->attrib['min_size']) ? '' : ($this->attrib['min_size']. ($this->attrib['min_size'] == $this->attrib['max_size'] ? '' :  '-'. $this->attrib['max_size']). ' 位的'). $this->get_error_type($this->attrib['type']));
        $string = '';
        $string .= "$prefix". (isset($this->attrib['label']) ? $this->attrib['label'] : 'UN') . (!$_t ? '' : "$suffix$_t");
        return $string;
    }

}

class form_widget_input_text extends form_widget_input
{
    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        return 
            $this->gen_label() .
            '<input type="text"'.$this->gen_attrib('name'). $this->gen_attrib('id').
            ' value="' . $this->gen_value('default_value', 'value'). '" ' .$this->gen_attrib('tabindex'). 
            $this->gen_attrib('class').  $this->gen_attrib('disabled') . ' />';
    }
}

class form_widget_input_textarea extends form_widget_input
{
    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        return 
            $this->gen_label() .
            '<textarea'.$this->gen_attrib('name'). $this->gen_attrib('id').
            $this->gen_attrib('tabindex').  $this->gen_attrib('class').
            $this->gen_attrib('rows') . $this->gen_attrib('cols').
            $this->gen_attrib('disabled') . '>'. $this->gen_value() . '</textarea>';
    }
}

/*
 * Ckeditor with ckfinder
 */
class form_widget_input_ckeditor extends form_widget_input
{
    protected function get_root_path()
    {
        return $_SERVER['DOCUMENT_ROOT'];
    }

    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        $language = isset($this->attrib['widget_info']['lang']) ?
                    $this->attrib['widget_info']['lang'] : 'zh-cn';
        $width = isset($this->attrib['widget_info']['width']) ?
                 $this->attrib['widget_info']['width'] : '100%';
        $toolbar = isset($this->attrib['widget_info']['toolbar']) ?
            $this->attrib['widget_info']['toolbar'] : '';

        $config =
            "{language : '$language', ". 
            ($toolbar ? "toolbar: '$toolbar', " : ''). 
            "width : '$width'}";

        $relative_dir = strlen(MB_DR) == 1 ? '' : MB_DR;
        $relative_dir = str_replace(array('\\', '//'), array('/', '/'), $relative_dir);
        $size = max(0, strlen($relative_dir) - 1);
        if (!$relative_dir || $relative_dir[$size] != '/')
            $relative_dir .= '/';

        $ckeditor_path = isset($this->attrib['widget_info']['ckeditor']) ? 
                         $this->attrib['widget_info']['ckeditor'] : 
                         $relative_dir. 'modules/ckeditor/ckeditor/ckeditor.js';
        $use_finder = isset($this->attrib['widget_info']['use_finder']) && 
                      !$this->attrib['widget_info']['use_finder'] ? false : true;

        if ($use_finder)
            $ckfinder_path = isset($this->attrib['widget_info']['ckfinder']) ?
                             $this->attrib['widget_info']['ckfinder'] :
                             $relative_dir . 'modules/ckeditor/ckfinder/ckfinder.js';
        $ckfinder_base = isset($this->attrib['widget_info']['ckfinder_base']) ?
                         $this->attrib['widget_info']['ckfinder_base'] :
                         $relative_dir . 'modules/ckeditor/ckfinder';

        $js_code = $this->gen_attrib('js_code');

        if (!$js_code)
            $js_code = sprintf(
                ($use_finder ? "var ckeditor = " : '').
                "CKEDITOR.replace('%s', %s); \n". 
                ($use_finder ? "CKFinder.setupCKEditor(ckeditor, '$ckfinder_base' ) ; ckeditor = null;" : ''),
                $this->attrib['name'],
                $config);


        return $this->gen_src_js($ckeditor_path). "\n" . ($use_finder ? $this->gen_src_js($ckfinder_path) . "\n" : ''). 
            $this->gen_label() .
            '<br /><textarea'. $this->gen_attrib('name'). $this->gen_attrib('id').
            $this->gen_attrib('tabindex'). $this->gen_attrib('class').
            '>{$valid.'.$this->attrib['name'].'.val}</textarea>'. $this->gen_inline_js($js_code);
    }
}

class form_widget_input_checkbox extends form_widget_input
{
    protected function gen_checked()
    {
        return (sprintf($this->if_isset_start,'valid['.$this->attrib['name'].']').
                ' checked="checked"'. $this->if_stop;
    }

    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        if (isset($this->attrib['widget_info']['multiple']) && $this->attrib['widget_info']['multiple']) {
        }

        return 
            $this->gen_label() .
            '<input type="checkbox"'.$this->gen_attrib('name'). $this->gen_attrib('id').
            $this->gen_checked(). 
            $this->gen_attrib('tabindex').  $this->gen_attrib('class').
            $this->gen_attrib('disabled') . ' />';
    }
}

class form_widget_input_radiobox extends form_widget_input_checkbox
{
    function _render()
    {
        if (!isset($this->attrib['name']) || !isset($this->attrib['widget_info']['values']))
            return '';
        $values = explode(';', $this->attrib['widget_info']['values']);
        $input = '';
        foreach ($values as $v) {
            $pos = explode(',', $v);
            $r_name = @array_shift($pos);
            $r_val = @array_shift($pos);
            $en = @array_shift($pos);
            $input .= '<input type="radio" '. $this->gen_attrib('name'). ' value="'. $r_val. '"'.
                '{if !$#valid.'.$this->attrib['name'].'.val#$ && "'. $en. '"} checked="checked"{else}{if $#valid.'. $this->attrib['name'] . '.val#$ == '. $r_val. '} checked="checked"{/if}{/if}' .
                ' /> '. $r_name . ' ';
        }
        return 
            $this->gen_label() . $input;
    }
}

class form_widget_input_select extends form_widget_input
{
    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        $select = $this->gen_label() .
            '<select'. $this->gen_attrib('name') . $this->gen_attrib('id').
            $this->gen_attrib('onchange'). $this->gen_attrib('onclick').
            $this->gen_attrib('class') . '>'. "\n";

        $select .= '<option value="0">请选择'.(isset($this->attrib['label']) ? $this->attrib['label'] : '').'</option>';
        $select .=
            '{loop $' . $this->attrib['name'] . ' name=_'.$this->attrib['name'] .'}'. "\n";

        $loop_key = isset($this->attrib['widget_info']['key_name']) ? $this->attrib['widget_info']['key_name'] : '';
        $loop_val = isset($this->attrib['widget_info']['val_name']) ? $this->attrib['widget_info']['val_name'] : '';
        $func = '';
        if (isset($this->attrib['widget_info']['val_func'])) {
            $func = preg_replace('/\$([a-z_\-]+)/i', '$_'.$this->attrib['name'].'[\1]', $this->attrib['widget_info']['val_func']);
            $func = '{func '.$func.'}';
        }

        $_loop_val = '$_' .$this->attrib['name']. ($loop_val ? '[\'' . $loop_val. '\']' : ''); 
        $select .= '<option value="{$_'.$this->attrib['name']. ($loop_val ? '['.$loop_val.']' : '') . '|escape}"'.
            '{if '.  '$#valid.'. $this->attrib['name'] . '.val#$' . ' == ' . 
            $_loop_val . /*(isset($this->attrib['widget_info']['selected']) ? 
            (' || '.  $this->attrib['widget_info']['selected']. ' == '. $_loop_val) : '') */
            '}'.
            ' selected="selected"{/if}>'.$func.'{$_'.$this->attrib['name']. ($loop_key ? '['.$loop_key.']' : '') . '|escape}</option>' . "\n".
            "{/loop}\n";
        $select .= "</select>\n";

        return $select;
    }
}

class form_widget_input_mselect_with_ajax extends form_widget_input
{
    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        $mselect = 
            $this->gen_label() .
            '<p id="'. $this->attrib['name']. '">&nbsp; ';
        return $mselect;
    }
}


class form_widget_input_file extends form_widget_input
{
    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        $mb_dir = MB_DR;
        if (substr($mb_dir, -1, 1) != '/')
            $mb_dir .= '/';

        $mb_dir = preg_replace('~/{2,5}~', '/', $mb_dir);

        /* Fixme */
        $upload_dir = $mb_dir. 'upload';
        $value = $this->gen_value();
        return 
            $this->gen_label() .
            '<input type="file"'.$this->gen_attrib('name'). $this->gen_attrib('id').
            $this->gen_attrib('tabindex').  $this->gen_attrib('class').
            $this->gen_attrib('maxsize').
            $this->gen_attrib('disabled') . ' />
            <input type="hidden" name="'.$this->attrib['name'].'_hidden" value="' . $value . '" />'.
            (!isset($this->attrib['widget_info']['preview']) ? '' : '<p><label class="light" for="preview'. 
            $this->get_default_id(). '">预览</label><img id="preview'. $this->get_default_id(). 
            '" alt="没有上传" align="top" src="'.$upload_dir . $value.'" /></p>');

    }
}

class form_widget_input_ajaxfile extends form_widget_input
{
    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';

        $url_root = web_utils::get_url_root();
        return $this->gen_label(). 
            $this->gen_src_js($url_root . '/static/js/ajaxupload.js'). "\n".
//            '<style type="text/css"> iframe { display:none; } </style>'.
            '<input type="file"'. $this->gen_attrib('name').' />'. "
            <button onclick=\"ajaxUpload(this.form,'". $url_root. "index.php/upload/do_upload/{$this->attrib['name']}/?maxSize=9999999999&amp;maxW=200&amp;&amp;colorR=255&amp;colorG=255&amp;colorB=255&amp;maxH=300','upload_area','图片上传中...&lt;br /&gt;&lt;img src=\'".
            $url_root. "static/img/loader_light_blue.gif\' width=\'128\' height=\'15\' border=\'0\' /&gt;','&lt;img src=\'".
            $url_root. "static/img/error.gif\' width=\'16\' height=\'16\' border=\'0\' /&gt; 上传失败.'); return false;\">上传图片</button>".
            '<p><label>上传预览</label><div id="upload_area">&nbsp;'.
            '{if $#ajax_uploaded_file#$ != ""}<a target="_blank" title="点击查看大图" href="{#url_root#}'. package::get_config('upload_img', 'path').'{$ajax_uploaded_file}">
                <img src="'.
            '{#url_root#}'. package::get_config('upload_img', 'path'). '{$ajax_uploaded_file|str_replace,".,_small."}" /></a>{/if}'.
            ' </div></p>';
    }
}

class form_widget_input_hidden extends form_widget_input
{
    function _render()
    {
        if (!isset($this->attrib['name']))
            return '';
        return '<input type="hidden"'. $this->gen_attrib('id') . $this->gen_attrib('name'). $this->gen_attrib('value'). ' />';
    }
}

class form_widget_linkage_select extends form_widget_base
{
    function _after()
    {
    }
    function _begore()
    {
    }
    function render()
    {
    }
    function _render()
    {
        $js = '<script type="text/javascript">';
    }
}

