<?php

package::import('form_upload');

class form_validate_plugin_form_upload extends form_validate_plugin
{
    function valid($var_name = '', $args = '', $submit_method = 'post')
    {
        $ret = $this->_process_upload($var_name, $args);
        if ($ret < 0)
            return null;
        return $ret;
    }

    protected function _process_upload($form_name, $args)
    {
        if (defined('MB_DR'))
            $upload_dir = MB_DR . (MB_DR ? '/' : '') . package::get_config('upload_img', 'path');
        else
            $upload_dir = './' . package::get_config('upload_img', 'path');

        $upload_dir = str_replace('\\', '/', $upload_dir);
        $upload_dir = preg_replace('~^/+~i', '', $upload_dir);

        $_args = array($upload_dir, '', 4096, 4096, 8192000);
        if (isset($args['max_width']))
            $_args[2] = $args['max_width'];
        if (isset($args['max_height']))
            $_args[3] = $args['max_height'];
        if (isset($args['max_capacity']))
            $_args[4] = $args['max_capacity'];
        if (isset($args['allowed_extension']))
            $_args[1] = $args['allowed_extension'];

        $form_upload = call_user_func_array(array('form_upload', '_'), $_args);
        if (!$form_upload)
            return -1;

        $form_upload->do_upload($form_name);
        $is_error = $form_upload->is_error();

        $config = loader::load_object('config', 'model');
        if (!$is_error && boot::$real_module && 
            ($thumb_width = $config->read(boot::$real_module. '_image_thumb_width')))
        {
            $thumb_file_pre = substr($form_upload->target_file, 0, -(strlen($form_upload->file_extension) + 1)); 
            package::import('image');
            $image = new image;
            $config_model = loader::load_object('config', 'model');
            $image->resize($form_upload->target_file, $thumb_file_pre. '_small.'. $form_upload->file_extension, $thumb_width);
            if ($thumbx_width = $config->read(boot::$real_module. '_image_thumbx_width')) {
                $image->resize($form_upload->target_file, $thumb_file_pre. '_smallx.'. $form_upload->file_extension, $thumbx_width);
            }
            if ($thumbm_width = $config->read(boot::$real_module. '_image_thumbm_width')) {
                $image->resize($form_upload->target_file, $thumb_file_pre. '_smallm.'. $form_upload->file_extension, $thumbm_width);
            }
        }

        /* hidden field set? */
        if ($is_error) {
            $hidden = mbstring::_("{$form_name}_hidden", array('method' => 'post'))->filter('gpc');
            if ($hidden)
                return $hidden;
        }
        return ($is_error ? (!isset($args['widget_info']['required']) || 
            !$args['widget_info']['required'] ? '' : -2) : $form_upload->file_path . $form_upload->file);
    }
}

?>
