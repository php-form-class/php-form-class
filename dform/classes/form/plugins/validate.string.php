<?php

require_once dirname(__FILE__). '/../../mbstring.php';

class form_validate_plugin_string extends form_validate_plugin
{
    protected
        $type_alias = array(
            'mobile' => 'string',
        );

    function valid($var_name = '', $args = array(), $submit_method = 'post')
    {
        if (!$var_name || !$args || !isset($args['type']))
            return null;

        $var = mbstring::_($var_name, array('method' => $submit_method));
        $required = !isset($args['widget_info']['required']) || !$args['widget_info']['required'] ? false : true;

        if (empty($var->string) && !$required) {
            $def = '';
            if (in_array($args['type'], array('integer', 'float', 'double', 'array')))
                settype($def, $args['type']);
            return $def;
        }

        if (!$var->validation($args['type'])) {
            return null;
        }

        if (((isset($args['min_size']) && $args['min_size'] !== '') || 
            (isset($args['max_size']) && $args['max_size'] !== '')) &&
            !$var->validation('size', $args['min_size'], $args['max_size'], isset($this->type_alias[$args['type']]) ? $this->type_alias[$args['type']] : $args['type']))
            return null;
        return $var->filter('gpc,'. $args['type']);
    }

}

?>
